const path = require('path')
const express = require('express')
const hbs = require('hbs')

const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

const app = express()
const port = process.env.PORT || 3000

// Define paths for Express config
const publicDirPath = path.join(__dirname, '../public/')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

// Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(publicDirPath))

// root
app.get('', (req, res) => {
	res.render('index', {
		title: 'Weather',
		name: 'johndrick'
	})
})

// about
app.get('/about', (req, res) => {
	res.render('about', {
		title: 'About Me',
		name: 'johndrick'
	})
})

// help
app.get('/help', (req, res) => {
	res.render('help', {
		title: 'Help Page',
		name: 'johndrick'
	})
})

// app.com/weather
app.get('/weather', (req, res) => {
	if (!req.query.address) {
		return res.send({
			error: 'You must provide an address'
		})
	}

	geocode(req.query.address, (error, { longitude, latitude, location } = {}) => {
		if (error) {
			return console.log('', error)
		}

		forecast(longitude, latitude, (error, forecastData) => {
			if (error) {
				return console.log('', error)
			}
			
			res.send({
                forecast: forecastData,
                location,
                address: req.query.address
            })
		})
	})
})

// experiment
app.get('/products', (req, res) => {
	if (!req.query.search) {
		return res.send({
			error: 'You must provide a search term'
		})
	}

	console.log	(req.query.search)
	res.send({
		products: []
	})
})

// 404 Route for help
app.get('/help/*', (req, res) => {
	res.render('404', {
		title: '404',
		message: 'Help article not found',
		name: 'johndrick'
	})
})

// 404 Route for everything else
app.get('*', (req, res) => {
	res.render('404', {
		title: '404',
		message: 'We can not find you have requested site!',
		name: 'johndrick'
	})
})

app.listen(port, () => {
	console.log('Server is up on port ' + port)
})