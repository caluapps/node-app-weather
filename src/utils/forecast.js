const request = require('request')

const forcast = (longitude, latitude, callback) => {
	const url = 'https://api.darksky.net/forecast/3ae91154a9540f72cf6ff2122df2f20c/' + longitude + ',' + latitude

	request({ url, json: true }, (error, { body }) => {
		if (error) {
			callback('Unable to connect to weather services!', undefined)

		} else if (body.error) {
			callback('Unable to find location. Try another search.', undefined)

		} else {
			callback(undefined, body.daily.data[0].summary + ' It is currently ' + body.currently.temperature + ' degress out. There is a ' + body.currently.precipProbability + '% chance of rain. Time: ' + new Date(body.currently.time).toLocaleTimeString())
		}
	})
}

module.exports = forcast